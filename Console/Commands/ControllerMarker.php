<?php

namespace Pengsqian\LaravelWechat\Console\Commands;

use \Illuminate\Routing\Console\ControllerMakeCommand as Command;
use Illuminate\Support\Str;

class ControllerMarker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'swechat:make-controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '微信组件创建控制器';


    protected $namespace = 'Pengsqian\LaravelWechat';
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->qualifyClass($this->getNameInput());


        $path = $this->getPath($name);
        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
        if ((! $this->hasOption('force') ||
                ! $this->option('force')) &&
            $this->alreadyExists($this->getNameInput())) {
            $this->error($this->type.' already exists!');

            return false;
        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type.' created successfully.');
    }

    protected function rootNamespace()
    {
        return $this->namespace;
    }

    protected function getPath($name)
    {
        $name = Str::replaceFirst($this->namespace, '', $name);

        return realpath(__DIR__ .'/../../') . str_replace('\\', '/', $name).'.php';
    }
}
