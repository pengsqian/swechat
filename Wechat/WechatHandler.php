<?php

namespace Pengsqian\LaravelWechat\Wechat;

use Pengsqian\LaravelWechat\Wechat\Msg\TextMsg;

class WechatHandler
{
    protected $msgType;
    protected $eventType;
    protected $payload = [];

    public function __construct($msgType, $payload)
    {

        $this->msgType = $msgType;
        $this->payload = $payload;

        if ($this->isEvent()) {
            $this->eventType = $payload['Event'];
        }

    }

    public function isEvent()
    {
        return $this->msgType === 'event';
    }

    public function handle()
    {
        if (method_exists($this, $handle = 'handle' . $this->msgType)) {
            return call_user_func([$this, $handle]);
        }

        return 'success';
    }

    public function handleText()
    {
        $content = $this->payload['Content'];
        $toUserName = $this->payload['ToUserName'];
        $fromUserName = $this->payload['FromUserName'];

        $content = "你好，你的消息是： $content";

        $info = (new TextMsg())->setToUser($toUserName)
            ->setFromUser($fromUserName)
            ->setContent($content)
            ->getMsg();

        return $info;
    }

    public function handleEvent()
    {

    }
}