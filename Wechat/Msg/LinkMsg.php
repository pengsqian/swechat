<?php


namespace Pengsqian\LaravelWechat\Wechat\Msg;


class LinkMsg extends AbstractMsg
{
    protected $title;
    protected $description;
    protected $url;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }
}