<?php


namespace Pengsqian\LaravelWechat\Wechat\Msg;


abstract class AbstractMsg
{

    protected $toUser;
    protected $fromUser;
    protected $createdTime;
    protected $msgType;
    protected $msgId;

    /**
     * @return mixed
     */
    public function getToUser()
    {
        return $this->toUser;
    }

    /**
     * @param mixed $toUser
     * @return static
     */
    public function setToUser($toUser)
    {
        $this->toUser = $toUser;

        return $this;
    }

    /**
     * @param mixed $fromUser
     * @return static
     */
    public function setFromUser($fromUser)
    {
        $this->fromUser = $fromUser;

        return $this;
    }

    /**
     * @param mixed $createdTime
     * @return static
     */
    public function setCreatedTime($createdTime = null)
    {
        $this->createdTime = $createdTime ?: time();

        return $this;
    }

    /**
     * @param mixed $msgType
     * @return static
     */
    public function setMsgType($msgType): self
    {
        $this->msgType = $msgType;

        return $this;
    }

    /**
     * @param mixed $msgId
     * @return static
     */
    public function setMsgId($msgId): self
    {
        $this->msgId = $msgId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFromUser()
    {
        return $this->fromUser;
    }

    /**
     * @return mixed
     */
    public function getCreatedTime()
    {
        return $this->createdTime ?: time();
    }

    /**
     * @return mixed
     */
    public function getMsgType()
    {
        return $this->msgType;
    }


    /**
     * @return mixed
     */
    public function getMsgId()
    {
        return $this->msgId;
    }

    public function getTplData()
    {
        return [
            '%ToUserName'   => $this->getToUser(),
            '%FromUserName' => $this->getFromUser(),
            '%CreateTime'   => $this->getCreatedTime(),
            '%MsgType'      => $this->getMsgType(),
            '%MsgId'        => $this->getMsgId()
        ];
    }

    public function getMsg()
    {
        $tpl = $this->getTplByType($this->msgType);

        return strtr($tpl, $this->getTplData());
    }

    public function getTplByType($type)
    {
        return config('swechat.wechat_tmp.' . $type, '');
    }

}