<?php


namespace Pengsqian\LaravelWechat\Wechat\Msg;


class ImageMsg extends AbstractMsg
{
    protected $picUrl;
    protected $mediaId;

    /**
     * @return mixed
     */
    public function getPicUrl()
    {
        return $this->picUrl;
    }

    /**
     * @return mixed
     */
    public function getMediaId()
    {
        return $this->mediaId;
    }

    function getTplData()
    {
        return array_merge(
            parent::getTplData(),
            [
                '%Content' => $this->getContent()
            ]
        );
    }



}