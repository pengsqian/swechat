<?php


namespace Pengsqian\LaravelWechat\Wechat\Msg;


class TextMsg extends AbstractMsg
{
    protected $content;
    protected $msgType = 'text';

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    function getTplData()
    {
        return array_merge(
            parent::getTplData(),
            [
                '%Content' => $this->getContent()
            ]
        );
    }

    /**
     * @param mixed $content
     * @return static
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }


}