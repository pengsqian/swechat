<?php
namespace Pengsqian\LaravelWechat\Http\Controllers;

use Illuminate\Http\Request;
use Pengsqian\LaravelWechat\Wechat\WechatHandler;

class WechatController extends Controller
{
    public function index(Request $request)
    {
        $postArr = (array) $request['wechat'];

        return (new WechatHandler($postArr['MsgType'], $postArr))->handle();
    }

}
