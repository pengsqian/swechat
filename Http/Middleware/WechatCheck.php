<?php

namespace Pengsqian\LaravelWechat\Http\Middleware;

use Closure;

class WechatCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $signature = $request->input('signature');
        $timestamp = $request->input('timestamp');
        $nonce = $request->input('nonce');
        $echostr = $request->input('echostr');
        $token = "weixin";
        $tmpArr = array($token, $timestamp, $nonce);
        sort($tmpArr, SORT_STRING);
        $tmpStr = implode( $tmpArr );
        $tmpStr = sha1( $tmpStr );

        return $next($request);
        if( $tmpStr == $signature ){
            if (empty($echostr)) {
                return $next($request);
            } else {
               return response($echostr);
            }
        }else{
            return response('签名失败');
        }

    }
}
