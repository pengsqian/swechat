<?php

namespace Pengsqian\LaravelWechat\Http\Middleware;

use Closure;

class WechatDataParse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $postObj =file_get_contents('php://input');
        $postArr = simplexml_load_string($postObj,"SimpleXMLElement",LIBXML_NOCDATA);

        if ($postArr === false) {
            return response('数据解析失败');
        }

        $request['wechat'] = $postArr;

        return $next($request);
    }
}
