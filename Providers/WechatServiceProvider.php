<?php

namespace Pengsqian\LaravelWechat\Providers;

use Illuminate\Support\ServiceProvider;
use Pengsqian\LaravelWechat\Console\Commands\ControllerMarker;
use Pengsqian\LaravelWechat\Http\Middleware\WechatCheck;
use Pengsqian\LaravelWechat\Http\Middleware\WechatDataParse;

class WechatServiceProvider  extends ServiceProvider
{
    protected $middlewareGroups = [];
    protected $routeMiddleware = [
        'swechat.check' => WechatCheck::class,
        'swechat.dataParse' => WechatDataParse::class
    ];
    protected $commands = [
        ControllerMarker::class
    ];

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/swechat.php',
            'swechat'
        );
        $this->registerMiddleware();
        $this->registerPublishes();
        $this->commands($this->commands);
    }

    public function boot()
    {
        $this->registerRoutes();
        $this->loadViewsFrom(
            __DIR__ . '/../Resources/views',
            'swechat'
        );

    }

    protected function registerPublishes()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../Config' => config_path()
            ], 'swechat');
        }
    }

    protected function registerMiddleware()
    {
        foreach ($this->middlewareGroups as $key => $middleware) {
            $this->app['router']->middlewareGroup($key, $middleware);
        }

        foreach ($this->routeMiddleware as $key => $middleware) {
            $this->app['router']->aliasMiddleware($key, $middleware);
        }
    }

    private function registerRoutes()
    {
        \Route::group(['namespace' => 'Pengsqian\LaravelWechat\Http\Controllers', 'prefix'=>'swechat'], function (){
            $this->loadRoutesFrom(__DIR__.'/../Http/routes.php');
        });

    }
}
