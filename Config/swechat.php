<?php
return [
    'wechat_tmp' => [
        // 文本模板
        'text'  => '
            <xml>
              <ToUserName><![CDATA[%ToUserName]]></ToUserName>
              <FromUserName><![CDATA[%FromUserName]]></FromUserName>
              <CreateTime>%CreateTime</CreateTime>
              <MsgType><![CDATA[text]]></MsgType>
              <Content><![CDATA[%Content]]></Content>
            </xml>',
        // 图片模板
        'image'  => '
            <xml>
              <ToUserName><![CDATA[%ToUserName]]></ToUserName>
              <FromUserName><![CDATA[%FromUserName]]></FromUserName>
              <CreateTime>%CreateTime</CreateTime>
              <MsgType><![CDATA[text]]></MsgType>
              <Image>
                  <MediaId><![CDATA[%MediaId]]></MediaId>
              </Image>
            </xml>',
        // 图文模板
        'news'  =>[
            'TplHead' => '
                  <xml>
                    <ToUserName><![CDATA[%ToUserName]]></ToUserName>
                    <FromUserName><![CDATA[%FromUserName]]></FromUserName>
                    <CreateTime>%CreateTime</CreateTime>
                    <MsgType><![CDATA[text]]></MsgType>
                    <ArticleCount>%s</ArticleCount>
                    <Articles>',
            'TplBody' => '
                    <item>
                        <Title><![CDATA[%s]]></Title>
                        <Description><![CDATA[%s]]></Description>
                        <PicUrl><![CDATA[%s]]></PicUrl>
                        <Url><![CDATA[%s]]></Url>
                    </item>',
            'TplFoot' => '
                    </Articles>
                  </xml>'
        ],
    ]

];
